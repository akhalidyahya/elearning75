<?php 
use CommonHelper\Helper;
?>
<div class="content-wrapper">
  <h4> <b>Ujian</b> <small class="text-muted">/ Tambah Bank Soal</small>
  </h4>
  <hr>
  <div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Form Tambah Siswa</h4>
              <form class="forms-sample" action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Nama Bank Soal</label>
                  <input name="name" type="text" class="form-control" placeholder="Misal: Geografi Bab 1" maxlength="150" required>
                </div>

                <div class="form-group">
                  <label for="kelas">Kelas</label>
                  <select class="form-control" required id="kelas" name="kelas">
                    <option value=''>-- Pilih --</option>
                    <?php

// $sqlKelas = mysqli_query($con, "SELECT * FROM tb_master_kelas ORDER BY id_kelas DESC");
//                     while ($jur = mysqli_fetch_array($sqlKelas)) {
//                       echo "<option value='$jur[id_kelas]'>$jur[kelas]</option>";
//                     }
                      $kelass = Helper::getKelasByGuru($_SESSION['Guru']);
                      foreach($kelass as $kelas) {
                        echo "<option value='".$kelas->id."'>".$kelas->kelas."</option>";
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="mapel">Mapel</label>
                  <select class="form-control" required id="mapel" name="mapel">
                    <option value=''>-- Pilih --</option>
                    <?php
                      $mapels = Helper::getMapelByGuru($_SESSION['Guru']);
                      // $sqlMapel = mysqli_query($con, "SELECT * FROM tb_master_mapel ORDER BY id_mapel DESC");
                      // while ($jur = mysqli_fetch_array($sqlMapel)) {
                      //   echo "<option value='$jur[id_mapel]'>$jur[mapel]</option>";
                      // }
                      foreach($mapels as $mapel) {
                        echo "<option value='".$mapel->id."'>".$mapel->mapel."</option>";
                      }
                    ?>
                  </select>
                </div>

                <button name="saveBankSoal" type="submit" class="btn btn-success mr-2">Submit</button>
                <button onclick='goBack()' class="btn btn-light">Cancel</button>
              </form>

              <?php

              if (isset($_POST['saveBankSoal'])) {

                $data = [
                  'mapel_id'    => $_POST['mapel'],
                  'kelas_id'    => $_POST['kelas'],
                  'name'        => $_POST['name'],
                  'created_at'  => date('Y-m-d H:i:s'),
                ];
                $save = mysqli_query($con, "INSERT INTO bank_soal VALUES(NULL,'".$data['mapel_id']."','".$data['kelas_id']."','".$data['name']."','".$data['created_at']."')");
                if ($save) {
                  echo " <script>
                    alert('Data Berhasil disimpan !');
                    window.location='?page=banksoal';
                    </script>";
                } else {
                  echo 'something went wrong';
                }
              }

              ?>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
  function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

      return false;
    return true;
  }
</script>
<script>
  function goBack() {
    window.history.back();
  }
</script>