<div class="content-wrapper">
    <h4> <b>Ujian</b> <small class="text-muted">/ Bank Soal</small>
    </h4>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <p class="card-description">
                <a href="?page=banksoal&act=add" class="btn btn-info text-white pull-right"><i class="fa fa-plus"></i> Add</a> <br>
                </p>
                <h4 class="card-title">Data Bank Soal</h4>
                    <div class="table-responsive">
                        <table class="table table-condensed table-striped table-hover" id="data">
                        <thead>
                        <tr>
                        <th>No.</th> 
                        <th>Nama Bank Soal</th> 
                        <th>Kelas</th> 
                        <th>Mapel</th> 
                        <th>Opsi</th>                     
                        </tr>                        
                        </thead>  
                        <tbody>
                        <?php 
                        $no=1;
                        $sql = mysqli_query($con,"SELECT bank_soal.name, bank_soal.id as id, tb_master_mapel.mapel, tb_master_kelas.kelas FROM bank_soal
                            LEFT JOIN tb_master_mapel ON bank_soal.mapel_id=tb_master_mapel.id_mapel
                            LEFT JOIN tb_master_kelas ON bank_soal.kelas_id=tb_master_kelas.id_kelas
                         ORDER BY created_at DESC");
                        foreach ($sql as $d) { ?>
                        <tr>
                            <td width="50"><b><?=$no++; ?>.</b> </td>
                            <td><?=$d['name']?> </td>
                            <td><?=$d['kelas']?> </td>
                            <td><?=$d['mapel']?> </td>
                            <td>
                                <a href="?page=banksoal&act=soal&banksoal=<?=$d['id']?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> Pertanyaan</a>
                                <a href="?page=banksoal&act=edit&id=<?=$d['id']?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                <a href="?page=banksoal&act=del&id=<?=$d['id']?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Del</a>
                            </td>                        
                        </tr>  
                        <?php } ?>                      
                        </tbody>                      
                        </table>                    
                    </div>
                </div>
            </div>                  
        </div>
    </div>
</div>
