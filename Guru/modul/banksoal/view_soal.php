<?php
use CommonHelper\Helper;
$sql = mysqli_query($con, "SELECT * FROM bank_soal WHERE id = '".$_GET['banksoal']."' ") or die(mysqli_error($con));
$bank_soal = mysqli_fetch_array($sql);
?>
<div class="content-wrapper">
  <h4>
    <b><?php echo $bank_soal['name']; ?></b>
    <small class="text-muted">/
      Data Soal
    </small>
  </h4>
  <div class="row purchace-popup">
    <div class="col-md-12">
      <span class="d-flex alifn-items-center">
        <a href="?page=ujian&act=soaladd&ID=<?= $_GET['banksoal']; ?>" class="btn btn-dark"> <i class="fa fa-plus text-white"></i> Add Soal</a>
        <button type='button' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_upload'> <i class='fa fa-file-excel-o text-white'></i> Upload File Excel
        </button>
        <a href="../Report/soal/print_soal.php?ID=<?= $_GET['banksoal']; ?>" target="_blank" class="btn btn-danger"> <i class="fa fa-download text-white"></i> Download Soal</a>

      </span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Daftar Soal <?php echo $bank_soal['name']; ?></h4>
          <div class="table-responsive">
            <table class='table table-striped' id="data">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Soal</th>
                  <!-- <th>Pilihan Ganda</th> -->
                  <th>Jawaban</th>
                  <th>Bobot</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $nomor = 1;
                $tampil = mysqli_query($con, "SELECT * FROM soal WHERE banksoal_id=" . $_GET['banksoal'] . " ORDER BY id_soal ASC");
                while ($r = mysqli_fetch_array($tampil)) { ?>

                  <tr>
                    <td><?= $nomor++; ?></td>
                    <td><?= Helper::cutString($r['soal']) ?></td>
                    <!-- <td>
                      <ol type='A'> -->
                        <?php
                        // for ($i = 1; $i <= 5; $i++) {
                        //   $kolom = "pilihan_$i";
                        //   $answer = strip_tags($r[$kolom]);
                        //   if ($r['kunci'] == $i) {
                        //     echo "<li style='font-weight: bold'>$answer</li>";
                        //   } else {
                        //     echo "<li>$answer</li>";
                        //   }
                        // }

                        ?>
                      <!-- </ol>
                    </td> -->
                    <td>
                      <?php 
                        echo Helper::ANSWER_LIST[$r['kunci']].'. '; 
                        for ($i = 1; $i <= 5; $i++) {
                          $kolom = "pilihan_$i";
                          $answer = Helper::cutString($r[$kolom],'jawaban');
                          if ($r['kunci'] == $i) {
                            echo $answer;
                          }
                        }
                      ?>
                    </td>
                    <td><?php echo $r['bobot']; ?></td>
                    <td>
                      <a href="?page=ujian&act=soaledit&ids=<?= $r['id_soal']; ?>&banksoal=<?= $_GET[banksoal]; ?>" class='btn btn-dark btn-sm'><i class='fa fa-pencil'></i></a>
                      <a href="?page=ujian&act=soaldel&ids=<?= $r['id_soal']; ?>&banksoal=<?= $_GET[banksoal]; ?>" class='btn btn-dark btn-sm text-danger'><i class='fa fa-trash'></i></a>
                    </td>
                  </tr>
                <?php
                }
                ?>
              </tbody>

            </table>




          </div>

        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>

<!-- Modal uploa xls -->
<div class='modal modal-info fade' id="modal_upload">
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-header'>
        <h4 class='modal-title'>Upload EXCEL</h4>
      </div>
      <form action="?page=ujian&act=upSoal" enctype="multipart/form-data" method="post">
        <input type="hidden" name="banksoal" value="<?= $_GET['banksoal']; ?>">
        <div class='modal-body'>
          <div class='form-group has-feedback'>
            <input type="file" class="file" id="file" name="excel" class="form-control" required>
          </div>
        </div>
        <div class="modal-footer">
          <a href="../download/soal_template.xlsx" class="btn btn-success pull-left"><i class="fa fa-file-excel-o"></i> contoh excel</a>
          <button name="uploadSoal" type="submit" class="btn btn-primary btn-save"><i class="fa fa-upload"></i> Upload</button>


        </div>
      </form>
    </div>

    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->