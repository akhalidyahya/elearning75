<?php

use CommonHelper\Helper;

$edit = mysqli_query($con, "SELECT * FROM tb_materi WHERE id_materi='$_GET[ID]' ") or die(mysqli_error($con));
$d = mysqli_fetch_array($edit);
?>

<div class="content-wrapper">
  <h4>
    Materi <small class="text-muted">/ Ubah</small>
  </h4>
  <hr>
  <div class="row">

    <div class="col-md-10 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Materi Pembelajaran</h4>
              <p class="card-description">
                <!-- Basic form layout -->
              </p>
              <form class="forms-sample" action="?page=proses" method="post" enctype="multipart/form-data">
                <input type="hidden" name="ID" value="<?= $d['id_materi']; ?>">
                <!-- <div class="form-group">
                  <label for="mapel">Mata Pelajaran</label>
                  <select class="form-control" name="id_roleguru" onchange="cek_database()" id="id_roleguru" style="width: 400px;font-weight: bold;background-color: #212121;color: #fff;" required>
                    <option>-- Pilih --</option> -->
                    <?php
                    // $sqlMapel = mysqli_query($con, "SELECT * FROM tb_roleguru
                    //       INNER JOIN tb_master_kelas ON tb_roleguru.id_kelas=tb_master_kelas.id_kelas
                    //       INNER JOIN tb_master_mapel ON tb_roleguru.id_mapel=tb_master_mapel.id_mapel
                    //       INNER JOIN tb_master_semester ON tb_roleguru.id_semester=tb_master_semester.id_semester
                    //       INNER JOIN tb_master_jurusan ON tb_roleguru.id_jurusan=tb_master_jurusan.id_jurusan
                    //       WHERE tb_roleguru.id_guru='$sesi'");
                    // while ($mapel = mysqli_fetch_array($sqlMapel)) {
                    //   if ($mapel['id_roleguru'] == $d['id_roleguru']) {
                    //     $selected = "selected";
                    //   } else {
                    //     $selected = "";
                    //   }
                    //   //echo "<option value='$jur[id_jurusan]' $selected>$jur[jurusan]</option>";
                    //   echo "<option value='$mapel[id_roleguru]' $selected>$mapel[mapel] - $mapel[kelas]- Jurusan $mapel[jurusan] - $mapel[semester]</option>";
                    // }
                    ?>
                  <!-- </select>

                  <input type="hidden" name="id_kelas" id="id_kelas">
                  <input type="hidden" name="id_mapel" id="id_mapel">
                  <input type="hidden" name="id_semester" id="id_semester">
                  <input type="hidden" name="id_jurusan" id="id_jurusan">
                </div> -->
                <div class="form-group">
                  <label for="mapel">Mata Pelajaran</label>
                  <div class="row">
                    <?php
                    $no = 1;
                    $sqlMapel = mysqli_query($con, "SELECT * FROM tb_roleguru
                            INNER JOIN tb_master_kelas ON tb_roleguru.id_kelas=tb_master_kelas.id_kelas
                            INNER JOIN tb_master_mapel ON tb_roleguru.id_mapel=tb_master_mapel.id_mapel
                            INNER JOIN tb_master_semester ON tb_roleguru.id_semester=tb_master_semester.id_semester
                            INNER JOIN tb_master_jurusan ON tb_roleguru.id_jurusan=tb_master_jurusan.id_jurusan
                            WHERE tb_roleguru.id_guru='$sesi'");
                    while ($mapel = mysqli_fetch_array($sqlMapel)) {
                      $no++;
                    ?>
                      <div class="col-md-4">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="<?php echo $mapel['id_roleguru']; ?>" name="id_roleguru[]"
                              <?php $existing_data = Helper::getRoleGuruByMateriId($_GET['ID']);
                              foreach($existing_data as $data) {
                                if($data->roleguru_id == $mapel['id_roleguru']) {
                                  echo 'checked';
                                }
                              }
                              ?>
                              > <?php echo "<b>$mapel[mapel]</b> - <b>$mapel[kelas]</b> - <b>$mapel[jurusan]</b> - <b>$mapel[semester]</b>" ?>
                          </label>
                        </div>
                      </div>
                    <?php
                      if ($no % 3 == 0) {
                        echo '<div class="clearfix"></div>';
                      }
                    } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="judul">Judul Materi</label>
                  <input type="text" id="judul" name="judul" class="form-control" value="<?= $d['judul_materi']; ?>">
                </div>
                <div class="form-group">
                  <label for="ckeditor">Materi</label>
                  <textarea name="materi" id="ckeditor"><?= $d['materi']; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="file">File Materi</label>
                  <?php if(!empty($d['file'])): ?>
                  <p>
                    <a href="<?= $d['file']; ?>" target="_blank" class="btn btn-primary btn-md text-white"><i class="fa fa-download"></i> Download</a>
                    <input type="hidden" value="<?= $d['file']; ?>" name="existingFile">
                    <a onclick="if(confirm('Ingin hapus file?'))location.href='?page=materi&act=delFile&id=<?= $_GET['ID'] ?>'" href="javascript:;" class="text-danger"> <i class="fa fa-times"></i> delete file</a>
                  </p>
                  <?php endif; ?>
                  <p>
                    File yang bisa di Upload hanya file dengan ekstensi .doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .rar, .exe, .zip dan besar file (file size) maksimal hanya <b>2 MB</b>.
                  </p>
                  <input type="file" name="file" class="form-control" style="background-color: #212121;color: #fff;font-weight: bold;">
                </div>

                <button type="submit" name="materiUpdate" class="btn btn-info mr-2">Update</button>
                <a href="javascript:history.back()" class="btn btn-danger">Batal</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>