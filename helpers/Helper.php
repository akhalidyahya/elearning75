<?php
namespace CommonHelper;

class Helper {
    const ANSWER_LIST = [
        '','A','B','C','D','E'
    ];

    const JENIS_UJIAN_PUBLIC_LISTS = [
        1 => 'Ya',
        0 => 'Tidak',
    ];

    const PENILAIAN_BOBOT   = 'BOBOT';
    const PENILAIAN_AUTO    = 'AUTO';
    const PENILAIAN_LISTS   = [
        self::PENILAIAN_BOBOT   => 'Bobot Nilai',
        self::PENILAIAN_AUTO    => 'Otomatis (benar/soal x 100)',
    ];

    public static function cutString($string,$type = 'soal') {
        $length = 80;
        if($type == 'jawaban') {
            $length = 15;
        }
        if(strlen($string) < $length) {
            return strip_tags($string);
        }
        return substr(strip_tags($string),0,$length).'...';
    }

    public static function getKelasByGuru($id) {
        include '../config/db.php';
        
        $sqlKelas = mysqli_query($con, "SELECT DISTINCT kelas.id_kelas, kelas.kelas FROM tb_roleguru 
            LEFT JOIN tb_master_kelas kelas ON kelas.id_kelas = tb_roleguru.id_kelas
            WHERE tb_roleguru.id_guru = $id
            ORDER BY kelas.id_kelas DESC");
        $data = [];
        while ($jur = mysqli_fetch_array($sqlKelas)) {
            $data[] = [
                'id'    => $jur['id_kelas'],
                'kelas' => $jur['kelas'],
            ];
        }
        return json_decode(json_encode($data), FALSE);
    }

    public static function getMapelByGuru($id) {
        include '../config/db.php';
        
        $sqlMapel = mysqli_query($con, "SELECT mapel.id_mapel, mapel.mapel FROM tb_roleguru 
            LEFT JOIN tb_master_mapel mapel ON mapel.id_mapel = tb_roleguru.id_mapel
            WHERE tb_roleguru.id_guru = $id
            ORDER BY mapel.id_mapel DESC");
        $data = [];
        while ($jur = mysqli_fetch_array($sqlMapel)) {
            $data[] = [
                'id'    => $jur['id_mapel'],
                'mapel' => $jur['mapel'],
            ];
        }
        return json_decode(json_encode($data), FALSE);
    }

    public static function getBankSoalByKelasGuru() {
        include '../config/db.php';
        
        $current_sesssion_teacher = false;
        if(array_key_exists('Guru',$_SESSION) && !empty(@$_SESSION['Guru'])) {
            $current_sesssion_teacher = true;
        }

        if($current_sesssion_teacher) {
            $sql_query = "SELECT * FROM bank_soal WHERE mapel_id IN 
                ( SELECT mapel.id_mapel FROM tb_roleguru 
                LEFT JOIN tb_master_mapel mapel ON mapel.id_mapel = tb_roleguru.id_mapel
                WHERE tb_roleguru.id_guru = ".$_SESSION['Guru']."
                ORDER BY mapel.id_mapel DESC ) ";
        } else {
            $sql_query = "SELECT * FROM bank_soal";
        }

        $sqlExec = mysqli_query($con, $sql_query);
        $data = [];
        while ($row = mysqli_fetch_array($sqlExec)) {
            $data[] = [
                'id'    => $row['id'],
                'name'  => $row['name'],
            ];
        }
        
        return json_decode(json_encode($data), FALSE);
    }

    public static function getNilaiSiswaById($id,$id_ujian)
    {
        include '../config/db.php';
        $sql_query = "SELECT nilai FROM nilai WHERE id_siswa = $id AND id_ujian = $id_ujian";
        $sqlExec = mysqli_query($con, $sql_query);
        $row = mysqli_fetch_array($sqlExec);
        if(empty($row)) {
            return ['nilai' => 0];
        }
        return $row;
    }

    public static function getNilaiSiswaByUjian($id_ujian)
    {
        include '../config/db.php';
        $sql_query = "SELECT id_siswa,nilai FROM nilai WHERE id_ujian = $id_ujian";
        $sqlExec = mysqli_query($con, $sql_query);
        $data = [];
        while ($row = mysqli_fetch_array($sqlExec)) {
            $data[] = [
                'id_siswa'  => $row['id_siswa'],
                'nilai'      => $row['nilai'],
            ];
        }
        return $data;
    }

    public static function getCurrentSiswaClass()
    {
        include '../config/db.php';
        $sql_query = "SELECT k.kelas, j.jurusan FROM tb_siswa s
                        LEFT JOIN tb_master_kelas k ON k.id_kelas = s.id_kelas
                        LEFT JOIN tb_master_jurusan j ON j.id_jurusan = s.id_jurusan
                        WHERE s.id_siswa = $_SESSION[Siswa]";
        $sqlExec = mysqli_query($con, $sql_query);
        $data = mysqli_fetch_array($sqlExec);
        return [
            'kelas'     => $data['kelas'],
            'jurusan'   => $data['jurusan']
        ];
    }

    public static function getClassByMateri($materi_id)
    {
        include '../config/db.php';
        $sql_query = "SELECT tb_master_kelas.kelas,tb_master_semester.semester, tb_master_mapel.mapel, tb_master_jurusan.jurusan
            FROM tb_materi
            LEFT JOIN materi_role_guru ON materi_role_guru.materi_id = tb_materi.id_materi
            INNER JOIN tb_roleguru ON materi_role_guru.roleguru_id=tb_roleguru.id_roleguru
            INNER JOIN tb_master_kelas ON tb_roleguru.id_kelas=tb_master_kelas.id_kelas
            INNER JOIN tb_master_mapel ON tb_roleguru.id_mapel=tb_master_mapel.id_mapel
            INNER JOIN tb_master_semester ON tb_roleguru.id_semester=tb_master_semester.id_semester
            INNER JOIN tb_master_jurusan ON tb_roleguru.id_jurusan=tb_master_jurusan.id_jurusan 
            WHERE tb_materi.id_materi=$materi_id ORDER BY id_materi DESC ";
        $sqlExec = mysqli_query($con, $sql_query);
        $data = [];
        while ($row = mysqli_fetch_array($sqlExec)) {
            $data[] = $row;
        }
        return $data;
    }

    public static function getRoleGuruByMateriId($materi_id)
    {
        include '../config/db.php';
        $sql_query = "SELECT * FROM materi_role_guru WHERE materi_id = $materi_id";
        $sqlExec = mysqli_query($con, $sql_query);
        $data = [];
        while ($row = mysqli_fetch_array($sqlExec)) {
            $data[] = $row;
        }
        if(empty($data)) {
            return [];
        }
        return json_decode(json_encode($data), FALSE);
    }

    public static function getAdminPhoneNumber()
    {
        include '../config/db.php';
        $sql_query = "SELECT * FROM tb_sekolah";
        $sqlExec = mysqli_query($con, $sql_query);
        $data = mysqli_fetch_array($sqlExec);
        return $data['no_admin'];
    }
}

?>